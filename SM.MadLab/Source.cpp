#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void Lyrics(string inputs[12]) {
	cout << endl << inputs[0] << " times, we fought that beast, your old man and me. \nIt had a " << inputs[1] << " head with " << inputs[2] << " feet, with a " << inputs[3] << "'s face too. (Aw that's rad!) \nAnd it was waiting in the bushes for us, then it " << inputs[4] << " off your dad's " << inputs[5] << ". \nHe was screaming something " << inputs[6] << ". In fact, there was this huge " << inputs[7] << " and I had to change the " << inputs[8] << ".\nThe " << inputs[8] << "? \nYou see his blood, it " << inputs[9] << " into the " << inputs[10] << " and I had to change them. \nBut we all got a " << inputs[1] << " " << inputs[2] << " " << inputs[3] << " thing waiting for us.\nEveryday I " << inputs[11] << " all day, about what's waiting in the bushes of love.\nSomething's waiting in the bushes for us. Something's waiting in the bushes of love.\nEveryday I " << inputs[11] << " all day, about what's waiting in the bushes of love.\nSomething's waiting in the bushes for us. Something's waiting in the bushes of love.\n";
}

void Writer(string inputs[12], ostream& os = cout) {
	os << endl << inputs[0] << " times, we fought that beast, your old man and me. \nIt had a " << inputs[1] << " head with " << inputs[2] << " feet, with a " << inputs[3] << "'s face too. (Aw that's rad!) \nAnd it was waiting in the bushes for us, then it " << inputs[4] << " off your dad's " << inputs[5] << ". \nHe was screaming something " << inputs[6] << ". In fact, there was this huge " << inputs[7] << " and I had to change the " << inputs[8] << ".\nThe " << inputs[8] << "? \nYou see his blood, it " << inputs[9] << " into the " << inputs[10] << " and I had to change them. \nBut we all got a " << inputs[1] << " " << inputs[2] << " " << inputs[3] << " thing waiting for us.\nEveryday I " << inputs[11] << " all day, about what's waiting in the bushes of love.\nSomething's waiting in the bushes for us. Something's waiting in the bushes of love.\nEveryday I " << inputs[11] << " all day, about what's waiting in the bushes of love.\nSomething's waiting in the bushes for us. Something's waiting in the bushes of love.\n";
}

int main() {
	string inputs[12] = { "a positive number: ", "a living thing: ", "a living thing: ", "a living thing: ", "a past tense verb: ", "a part of the body: ", "an adjective: ", "an idea(noun): ", "a plural noun: ", "a past-tense verb: ", "an object: ", "a current-tense verb: "};
	
	for (int i = 0; i < 12; i++) {
		cout << "Enter " << inputs[i];
		cin >> inputs[i];
	}

	Lyrics(inputs);
	string path = "C:\\Users\\Public\\MadLib.txt";
	string yesno;
	cout << endl << "Do you want to save this Mad Lib? Enter y for yes or n for no:";
	cin >> yesno;
	if (yesno == "Y" || yesno == "y")
	{
		ofstream ofs(path);
		Writer(inputs, ofs);
		ofs.close();
		cout << "Your mad lib has been saved to " << path << endl;
	}
	
	cout << "Have a nice day.";

	(void)_getch();
	return 0;
}

//This is the script from Bushes of Love - A Bad Lip-Reading of Star Wars
/*[positive number] times, we fought that beast, your old man and me
It had a [living thing1] head with [living thing2] feet, with a [living thing3]'s face too. (Aw, that's rad)
And it was waiting in the bushes for us, then it [past-tense verb] off your dad's [part of the body].
He was screaming something [adjective]. In fact there was this huge [noun idea] and I had to change the [plural noun1].
The [plural noun1]?
You see his blood, it [past-tense verb] into the [object]s and I had to change them.
But we all got a [living thing1] [living thing2] [living thing3] thing waiting for us.
Everyday, I [current-tense verb] all day, about what's waiting in the bushes of love,
Something's waiting in the bushes for us, something's waiting in the bushes of love.
Everyday, I [current-tense verb] all day, about what's waiting in the bushes of love,
Something's waiting in the bushes for us, something's waiting in the bushes of love.*/